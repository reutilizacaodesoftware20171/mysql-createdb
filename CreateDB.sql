START TRANSACTION;

DROP TABLE IF EXISTS app_item;
DROP TABLE IF EXISTS app;

CREATE TABLE app (

	id    INTEGER      NOT NULL AUTO_INCREMENT COMMENT 'Identificador.',
    name  VARCHAR(100) NOT NULL                COMMENT 'Nome do App.',
    theme VARCHAR(20)  NOT NULL                COMMENT 'Cor do App',
    title VARCHAR(50)  NOT NULL                COMMENT 'T�tulo do Cabe�alho',
    label VARCHAR(200) NOT NULL                COMMENT 'Texto exibido acima da lista.',

	PRIMARY KEY (id),
	UNIQUE (name),
    INDEX(name),
    INDEX(theme),
    INDEX(title),
    INDEX(label)

) ENGINE = InnoDB DEFAULT CHARACTER SET = latin1
COMMENT = 'Lista de poss�veis aplicativos';

CREATE TABLE app_item (

	id     INTEGER      NOT NULL AUTO_INCREMENT COMMENT 'Identificador.',
    item   VARCHAR(300) NOT NULL                COMMENT 'Item.',
    fk_app INTEGER      NOT NULL                COMMENT 'App ao qual o item se refere.',

	PRIMARY KEY (id),
    INDEX(item),
    INDEX(fk_app),
    FOREIGN KEY (fk_app) REFERENCES rollndev.app(id)

) ENGINE = InnoDB DEFAULT CHARACTER SET = latin1
COMMENT = 'Lista de itens referentes ao aplicativo.';


DELIMITER $$;

DROP PROCEDURE IF EXISTS createApp $$;
CREATE PROCEDURE createApp(IN appName_ VARCHAR(100), IN appTheme_ VARCHAR(20), IN appTitle_ VARCHAR(50), IN appLabel_ VARCHAR(200))
COMMENT 'Cria um novo aplicativo.'
BEGIN
	INSERT INTO app (name, theme, title, label) VALUES (appName_, appTheme_, appTitle_, appLabel_);
END $$;

DROP PROCEDURE IF EXISTS getApp $$;
CREATE PROCEDURE getApp(IN appName_ VARCHAR(100))
COMMENT 'Retorna os dados de um aplicativo.'
BEGIN
    SELECT theme, title, label from app where name = appName_;
END $$;

DROP PROCEDURE IF EXISTS getItems $$;
CREATE PROCEDURE getItems(IN appName_ VARCHAR(100))
COMMENT 'Retorna a lista de itens de um aplicativo.'
BEGIN
	DECLARE idApp INTEGER;
	SET idApp = (select id from app where name = appName_);
	
    SELECT item from app_item where fk_app = idApp ORDER BY item;
END $$;

DROP PROCEDURE IF EXISTS addItem $$;
CREATE PROCEDURE addItem(IN appName_ VARCHAR(100), IN item_ VARCHAR(300))
COMMENT 'Insere um novo item de um aplicativo.'
BEGIN
	DECLARE idApp INTEGER;
	SET idApp = (select id from app where name = appName_);
    
	INSERT INTO app_item (item, fk_app) VALUES (item_, idApp);
END $$;

DELIMITER ;

COMMIT;